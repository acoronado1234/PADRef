//
//  TableViewCell2.swift
//  PADRef
//
//  Created by Alexander Coronado on 8/17/18.
//  Copyright © 2018 Alexander Coronado. All rights reserved.
//

import UIKit

class TableViewCell2: UITableViewCell {

    @IBOutlet weak var MainImageView: UIImageView!
    
    @IBOutlet weak var MainLabel: UILabel!
}
