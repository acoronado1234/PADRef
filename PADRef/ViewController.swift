//
//  ViewController.swift
//  PADRef
//
//  Created by Alexander Coronado on 8/16/18.
//  Copyright © 2018 Alexander Coronado. All rights reserved.
//

import UIKit

struct cellData {
    
    let cell : Int!
    let text : String!
    let image : UIImage!

}

class TableViewController: UITableViewController, UISearchBarDelegate{
    
    var arrayOfCellData = [cellData]()
    
    override func viewDidLoad() {
        arrayOfCellData = [cellData(cell: 1, text: "abcd", image: #imageLiteral(resourceName: "dabbing-collection-7")), cellData(cell: 1, text: "edfg", image: #imageLiteral(resourceName: "dabbing-collection-7")), cellData(cell: 1, text: "hijk", image: #imageLiteral(resourceName: "dabbing-collection-7"))]
    }

    @IBOutlet weak var secondSearch: UISearchBar!
    
    func myFunc() {
        secondSearch.delegate = self
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayOfCellData.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //iterates through each row
        
        let cell = Bundle.main.loadNibNamed("TableViewCell1", owner: self, options: nil)?.first as! TableViewCell1
        
        cell.MainImageView.image = arrayOfCellData[indexPath.row].image
        cell.MainLabel.text = arrayOfCellData[indexPath.row].text
        
        return cell
        
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 50

    }

}

